//use sdl2::event;
//use sdl2::keyboard::Keycode;
//use sdl2::pixels::Color;
//use std::env;

//const WARNINGS: bool = true;
//const PIXEL_SCALE: i32 = 10;
//const WINDOW_WIDTH: i32 = cpu::GFX_WIDTH as i32 * PIXEL_SCALE;
//const WINDOW_HEIGHT: i32 = cpu::GFX_HEIGHT as i32 * PIXEL_SCALE;
//const FRAMERATE: u32 = 60;
//const SPEED: u32 = 500;

//const FOREGROUND_COLOR: Color = Color {
//    r: 179,
//    g: 139,
//    b: 255,
//    a: 255,
//};
//const BACKGROUND_COLOR: Color = Color {
//    r: 65,
//    g: 31,
//    b: 127,
//    a: 255,
//};

//const KEYMAP: [Keycode; 16] = [
//    Keycode::Num1, // 0
//    Keycode::Num2, // 1
//    Keycode::Num3, // 2
//    Keycode::Num4, // 3
//    Keycode::Q,    // 4
//    Keycode::W,    // 5
//    Keycode::E,    // 6
//    Keycode::R,    // 7
//    Keycode::A,    // 8
//    Keycode::S,    // 9
//    Keycode::D,    // A
//    Keycode::F,    // B
//    Keycode::Z,    // C
//   Keycode::X,    // D
//     Keycode::C,    // E
 //   Keycode::V,    // F
//];

fn main() {
    // let mut main_cpu = cpu::Chip8CPU::new();
    // let args: Vec<String> = env::args().collect();
    // let mut curkey: Option<u8> = None;
    // //Used to determine when to update the screen and execute timers
    // let mut fcount = 8;
    // if args.len() != 2 {
    //     usage();
    //     return;
    // }

    // //INIT SDL2
    // let ctx = match sdl2::init() {
    //     Ok(c) => c,
    //     Err(e) => panic!("Cannot initialize SDL: {}", e)
    // };
    // let mut events = match ctx.event_pump() {
    //     Ok(e) => e,
    //     Err(e) => panic!("Couldn't create event pump: {}", e)
    // };
    // //init video
    // let vid_ctx = match ctx.video() {
    //     Ok(c) => c,
    //     Err(e) => panic!("Cannot get video context: {}", e)
    // };
    // let main_window = match vid_ctx.window("Chip-8-rs", WINDOW_WIDTH as u32, WINDOW_HEIGHT as u32).position_centered().build() {
    //     Ok(w) => w,
    //     Err(e) => panic!("Failed to make window: {}", e)
    // };
    // //Init renderer
    // let mut canvas = match main_window.into_canvas().build() {
    //     Ok(r) => r,
    //     Err(e) => panic!("Failed to create main renderer: {}", e)
    // };
    // canvas.set_draw_color(Color::RGB(0, 0, 0));
    // canvas.clear();
    // canvas.present();

    // let filename = &args[1];
    // println!("Loading {}", filename);
    // main_cpu.reset();
    // match main_cpu.load(filename) {
    //     Ok(()) => (),
    //     Err(e) => {
    //         panic!(format!("Error: {}", e));
    //     }
    // }
    // println!("Loaded! Running {}", filename);
    // 'main: loop{
    //     for event in events.poll_iter() {
    //         match event {
    //             event::Event::Quit{..} => break 'main,
    //             event::Event::KeyDown{ keycode: Some(Keycode::Escape), ..} => {

    //             }
    //             event::Event::KeyDown{ keycode: Some(e), .. } => {
    //                 for (i, val) in KEYMAP.into_iter().enumerate() {
    //                     if *val == e {
    //                         curkey = Some(i as u8);
    //                     }
    //                 }
    //             }
    //             _ => {}
    //         }
    //     }
    //     if main_cpu.needs_redraw() {
    //         let gfx = main_cpu.get_graphics();
    //         canvas.set_draw_color(BACKGROUND_COLOR);
    //         canvas.clear();
    //         canvas.set_draw_color(FOREGROUND_COLOR);
    //         for (y, line) in gfx.into_iter().enumerate() {
    //             for (x, pixel) in line.into_iter().enumerate() {
    //                 if *pixel {
    //                     let rect = sdl2::rect::Rect::new(x as i32 * PIXEL_SCALE, y as i32 * PIXEL_SCALE, PIXEL_SCALE as u32, PIXEL_SCALE as u32);
    //                     canvas.fill_rect(rect).unwrap();
    //                 }
    //             }
    //         }
    //         main_cpu.set_drawn();
    //     }
    //     std::thread::sleep(std::time::Duration::new(0, 1_000_000_000u32 / SPEED));
    //     //std::thread::sleep(std::time::Duration::new(0, 60));
    //     match main_cpu.tick(curkey) {
    //         Err(e) => {
    //             panic!(e);
    //         }
    //         Ok(()) => {}
    //     }
    //     curkey = None;
    //     fcount += 1;
    //     if fcount >= SPEED / FRAMERATE {
    //         main_cpu.do_timers();
    //         canvas.present();
    //     }
    //     if WARNINGS {
    //         if main_cpu.warning() {
    //             println!("Warning: {}", main_cpu.warning_message());
    //         }
    //     }
    // }
}

//fn usage() {
//    println!("chip-8-rust: A CHIP-8 emulator written in Rust");
//    println!("usage: chip-8-rust <rom file>");
//}
