#![no_std]
#![warn(missing_docs)]
#![allow(dead_code)]
#![warn(clippy::pedantic)]

//! A library implementation of the famous CHIP-8 emulator.
//!
//! This version takes much of its information from [Cowgod's Chip-8 Technical Reference](http://devernay.free.fr/hacks/chip8/C8TECH10.HTM)
//!
//! The goal of this implementation is to have clean code along with an easy to implement interface
//!
//! A secondary goal is to make it `no_std` safe.


use nanorand::RNG;

#[cfg(test)]
mod tests;

/// This trait is used by the implementing program to emulate the screen.
pub trait Display {
    /// This is used to draw a pixel to the screen.
    ///
    /// When drawing a pixel to the screen in chip-8, it
    /// must XOR with any existing pixel that is there.
    ///
    /// For example: if there is a pixel already existing at
    /// (8,3) and the function requests to draw here,
    /// you must set this pixel to "false".
    ///
    /// If a coordinate is given that is out of range, fail silently,
    /// and do not draw anything
    ///
    /// This funciton will also return if true if any pixel
    /// ever overlaps another, as in the above case.
    fn draw_pixel(&mut self, x: usize, y: usize) -> bool;
    /// Reset the display to its completely initial state
    fn reset(&mut self);
    /// Just simply clear the screen. The states that are kept
    /// are completely up to implementation.
    ///
    /// It is recommended to just optimize it to do the absolute minimum
    /// to set the screen to fully blank.
    fn clear(&mut self);
    /// Should return the maximum width of the current emulator.
    /// This should always be 64, but can be changed, if you want. Expect
    /// breakage with programs.
    fn get_max_width(&self) -> usize {
        64
    }
    /// Should return the maximum height of the current emulator.
    /// This should always be 32, but can be changed, if you want. Expect
    /// breakage with programs.
    fn get_max_height(&self) -> usize {
        32
    }
}

/// This trait is used by the implementing program to emulate the keypad
pub trait Keypad {
    /// Returns true if the given key is pressed
    /// The key should never be larger than 0xF, and if it is,
    /// the implementation should either panic, or error gracefully
    fn is_key_pressed(&self, key: u8) -> bool;
}

/// This trait is used by the implementing program to emulate the sound
pub trait Sound {
    /// This function will set the sound as ringing.
    fn set_sound_ringing(&self);
    /// This function will disable the sound
    fn set_sound_silent(&self);
}

/// This enum is used as a return value for the tick
/// function of the CPU.
#[derive(Debug)]
pub enum CpuReturn {
    /// Returned when there are no errors
    Ok,
    /// Returned when there is a non-fatal
    /// warning for the program.
    ///
    /// Returns a tuple of the current PC
    /// (program counter) of the CPU and
    /// an enum describing which warning
    /// is being emitted
    Warn((usize, CpuWarning)),
    /// Returned when there is a fatal
    /// error in the program
    /// If this variation is ever emitted,
    /// then the program should be immediately
    /// halted. The CPU will not do this automatically
    ///
    /// Returns a tuple of the current PC
    /// (program counter) of the CPU and
    /// an enum describing which error is
    /// being emitted.
    Error((usize, CpuError)),
}

/// An Enum describing which error is currently being
/// emitted by the CPU
#[derive(Debug, PartialEq)]
pub enum CpuError {
    /// This error is emitted when the internal stack is
    /// underflowed by the running program
    StackUnderflow,
    /// This error is emitted when the internal stack is
    /// overflowed by the running program
    StackOverflow,
    /// This error is emitted when an invalid opcode is
    /// encountered. This variant contains the opcode
    /// in question.
    InvalidOpcode(u16),
    /// This error is emitted when an opcode is given an
    /// invalid value
    InvalidValue(u16),
}

/// An enum describing which warning is currently being
/// emitted by the CPU
#[derive(Debug, PartialEq)]
pub enum CpuWarning {
    /// This warning is emitted when the CPU encounters
    /// a SYS opcode, which in the orignal CHIP-8 design,
    /// ran the specified opcode on the host machine.
    ///
    /// It is recommended these opcodes be ignored, but
    /// can be implemented if desired.
    SysOpcode(u16),
}

/// An enum to store the current state of the CPU
#[derive(Debug, PartialEq)]
pub enum CurrentState {
    /// The CPU's normal state
    Running,
    /// The CPU has encountered an error
    Error,
    /// The CPU is currently waiting for a keypress
    /// Stores the register to store the keypress into
    WaitingForKeypress(usize)
}

const RAM_SIZE: usize = 4096;

/// A dummy type for the RAM of the Chip-8.
type Ram = [u8; RAM_SIZE];

/// The main CPU of the Chip 8 emulator
///
/// This will include the registers,
/// the ram, and also a reference to
/// the display implementation
pub struct Cpu<'a> {
    regs: Registers,
    ram: Ram,
    disp: &'a mut dyn Display,
    state: CurrentState,
    keypad: &'a mut dyn Keypad,
}

impl Cpu<'_> {
    const FONT: [u8; 80] = [
        0xF0, 0x90, 0x90, 0x90, 0xF0, //0
        0x20, 0x60, 0x20, 0x20, 0x70, //1
        0xF0, 0x10, 0xF0, 0x80, 0xF0, //2
        0xF0, 0x10, 0xF0, 0x10, 0xF0, //3
        0x90, 0x90, 0xF0, 0x10, 0x10, //4
        0xF0, 0x80, 0xF0, 0x10, 0xF0, //5
        0xF0, 0x80, 0xF0, 0x90, 0xF0, //6
        0xF0, 0x10, 0x20, 0x40, 0x40, //7
        0xF0, 0x90, 0xF0, 0x90, 0xF0, //8
        0xF0, 0x90, 0xF0, 0x10, 0xF0, //9
        0xF0, 0x90, 0xF0, 0x90, 0x90, //A
        0xE0, 0x90, 0xE0, 0x90, 0xE0, //B
        0xF0, 0x80, 0x80, 0x80, 0xF0, //C
        0xE0, 0x90, 0x90, 0x90, 0xE0, //D
        0xF0, 0x80, 0xF0, 0x80, 0xF0, //E
        0xF0, 0x80, 0xF0, 0x80, 0x80, //F
    ];
    /// Creates a new CPU with the given [Display](trait.Display.html)
    /// and [Keypad](trait.Keypad.html) implementation
    ///
    ///
    /// Returns:
    /// A new, uninitalized CPU. Must be initialized by [reset()](struct.Cpu.html#method.reset)
    pub fn new<'a>(disp: &'a mut dyn Display, keypad: &'a mut dyn Keypad) -> Cpu<'a> {
        Cpu {
            regs: Registers::new(),
            ram: [0; RAM_SIZE],
            disp,
            state: CurrentState::Running,
            keypad
        }
    }


    /// Will tick the timer registers of the Chip 8. This should
    /// happen every 60hz.
    ///
    /// Returns true when the sound timer is 0.
    pub fn tick(&mut self) -> bool {
        self.regs.sound = self.regs.sound.saturating_sub(1);
        self.regs.delay = self.regs.delay.saturating_sub(1);

        self.regs.sound == 0
    }

    /// Will complete one CPU cycle. This can happen at any speed, but generally you
    /// want to keep it around 500Hz (0.5MHz)
    /// Keep in mind the computer must recieve at 60hz (every 1/60 of a second)
    pub fn cycle(&mut self) -> CpuReturn {

        CpuReturn::Error((0, CpuError::StackOverflow))
    }

    /// Resets the CPU and the contained [Display](trait.Display.html)
    /// implementation.
    ///
    /// The reset process includes: - Resetting the registers to their intial
    /// states.
    /// - Clearing the RAM and setting up the
    /// default font
    /// - Resetting the display implentation
    pub fn reset(&mut self) {
        self.regs.reset();
        self.clear_ram();
        self.disp.reset();
    }

    /// Clears the RAM of the CPU and populates the font into the RAM
    fn clear_ram(&mut self) {
        for byte in self.ram.iter_mut() {
            *byte = 0;
        }
        // Populate the font
        for (count, byte) in Self::FONT.iter().enumerate() {
            self.ram[count] = *byte;
        }
    }

    /// Gets an x component from an opcode
    fn get_opcode_x(opcode: u16) -> usize {
        ((opcode & (0xF << 8)) >> 8) as usize
    }
    /// Gets a y component from an opcode
    fn get_opcode_y(opcode: u16) -> usize {
        ((opcode & (0xF << 4)) >> 8) as usize
    }
    /// Gets an nnn component from an opcode
    // The mask that I'm using makes it impossible for
    // truncation to occur
    #[allow(clippy::cast_possible_truncation)]
    fn get_opcode_nnn(opcode: u16) -> u16 {
        opcode & 0xFFF
    }
    /// Gets a kk component from an opcode
    // The mask that I'm using makes it impossible for
    // truncation to occur
    #[allow(clippy::cast_possible_truncation)]
    fn get_opcode_kk(opcode: u16) -> u8 {
        (opcode & 0xFF) as u8
    }
    /// Gets an n component from an opcode
    // The mask that I'm using makes it impossible for
    // truncation to occur
    #[allow(clippy::cast_possible_truncation)]
    fn get_opcode_n(opcode: u16) -> u8 {
        (opcode & 0xF) as u8
    }

    /// Opcode: CLS
    ///
    /// Clears the screen
    fn opcode_cls(&mut self) -> CpuReturn {
        self.disp.clear();
        CpuReturn::Ok
    }

    /// Opcode: RET
    ///
    /// Pops an address off the stack, and sets the current
    /// address to that address
    fn opcode_ret(&mut self) -> CpuReturn {
        if let Ok(addr) = self.regs.pop_from_stack() {
            self.regs.pc = addr;
            CpuReturn::Ok
        } else {
            CpuReturn::Error((self.regs.pc, CpuError::StackUnderflow))
        }
    }

    /// Opcode: SYS
    ///
    /// Runs a native opcode. Currently just runs nothing and warns the
    /// emulator
    fn opcode_sys(&mut self, opcode: u16) -> CpuReturn {
        // We just simply warn the program of a sysopcode being used
        CpuReturn::Warn((self.regs.pc, CpuWarning::SysOpcode(opcode)))
    }

    /// Opcode: JP (1)
    ///
    /// Sets the program counter to the address passed in the opcode
    fn opcode_jp1(&mut self, addr: usize) -> CpuReturn {
        self.regs.pc = addr;
        CpuReturn::Ok
    }

    /// Opcode: JP (2)
    ///
    /// Sets the program counter to the address passed in the opcode
    /// plus the value of V0
    fn opcode_jp2(&mut self, addr: usize) -> CpuReturn {
        self.regs.pc = addr + (self.regs.v[0x0] as usize);
        CpuReturn::Ok
    }

    /// Opcode: CALL
    ///
    /// Pushes the current program counter to the stack, then sets the program
    /// counter to the address passed in the opcode
    fn opcode_call(&mut self, addr: usize) -> CpuReturn {
        match self.regs.push_to_stack(self.regs.pc) {
            Ok(_) => {
                self.regs.pc = addr;
                CpuReturn::Ok
            },
            Err(_) => {
                CpuReturn::Error((self.regs.pc, CpuError::StackOverflow))
            }
        }
    }

    /// Opcode: SE (1)
    ///
    /// Skips an instruction if the byte in Vx matches the byte passed in
    /// the opcode
    fn opcode_se1(&mut self, vx: usize, byte: u8) -> CpuReturn {
        if self.regs.v[vx] == byte {
            self.regs.pc += 2;
        }
        CpuReturn::Ok
    }

    /// Opcode: SE (2)
    ///
    /// Skips an instruction if the  byte in Vx is equal to the byte in
    /// Vy
    fn opcode_se2(&mut self, vx: usize, vy: usize) -> CpuReturn {
        if self.regs.v[vx] == self.regs.v[vy] {
            self.regs.pc += 2;
        }
        CpuReturn::Ok
    }

    /// Opcode: SNE (1)
    ///
    /// Skips an instruction if the byte in Vx does not match the byte
    /// passed in the opcode
    fn opcode_sne1(&mut self, vx: usize, byte: u8) -> CpuReturn {
        if self.regs.v[vx] != byte {
            self.regs.pc += 2;
        }
        CpuReturn::Ok
    }

    /// Opcode: SNE (2)
    ///
    /// Skips an instruction if the byte in Vx does not match the byte in
    /// Vy
    fn opcode_sne2(&mut self, vx: usize, vy: usize) -> CpuReturn {
        if self.regs.v[vx] != self.regs.v[vy] {
            self.regs.pc += 2;
        }
        CpuReturn::Ok
    }

    /// Opcode: LD (1)
    ///
    /// Loads the byte passed by the opcode into Vx
    fn opcode_ld1(&mut self, vx: usize, byte: u8) -> CpuReturn {
        self.regs.v[vx] = byte;
        CpuReturn::Ok
    }

    /// Opcode: LD (2)
    ///
    /// Loads the value of Vy into Vx
    fn opcode_ld2(&mut self, vx: usize, vy: usize) -> CpuReturn {
        self.regs.v[vy] = self.regs.v[vx];
        CpuReturn::Ok
    }

    /// Opcode : LD (3)
    ///
    /// Loads the nnn value into the I register
    fn opcode_ld3(&mut self, addr: u16) -> CpuReturn {
        self.regs.i = addr;
        CpuReturn::Ok
    }

    /// Opcode: LD (4)
    ///
    /// Loads the value of Vx into the Delay Timer register
    fn opcode_ld4(&mut self, vx: usize) -> CpuReturn {
        self.regs.v[vx] = self.regs.delay as u8;
        CpuReturn::Ok
    }

    /// Opcode: LD (5)
    ///
    /// Waits for a key to be pressed, and stores the value of the key pressed in Vx
    fn opcode_ld5(&mut self, vx: usize) -> CpuReturn {
        self.state = CurrentState::WaitingForKeypress(vx);
        CpuReturn::Ok
    }

    /// Opcode LD (6)
    ///
    /// Sets the delay timer to the value in Vx
    fn opcode_ld6(&mut self, vx: usize) -> CpuReturn {
        self.regs.delay = self.regs.v[vx] as usize;
        CpuReturn::Ok
    }

    /// Opcode LD (7)
    ///
    /// Sets the sound timer to the value in Vx
    fn opcode_ld7(&mut self, vx: usize) -> CpuReturn {
        self.regs.sound = self.regs.v[vx] as usize;
        CpuReturn::Ok
    }

    /// Opcode LD (8)
    ///
    /// Sets I to the location for the sprite value in Vx
    fn opcode_ld8(&mut self, vx: usize) -> CpuReturn {
        self.regs.i = u16::from(self.regs.v[vx] & 0xF) * 5;
        CpuReturn::Ok
    }

    /// Opcode LD (9)
    ///
    /// Stores the BCD representation of Vx in the memory locations I, I+1, and I+2
    fn opcode_ld9(&mut self, vx: usize) -> CpuReturn {
        let mut vx = self.regs.v[vx];
        let i = self.regs.i as usize;
        // 100s digit
        self.ram[i] = vx / 100;
        vx = vx % 100;
        // 10s digit
        self.ram[i + 1] = vx / 10;
        vx = vx % 10;
        // 1s digit
        self.ram[i + 2] = vx;
        CpuReturn::Ok
    }

    /// Opcode: LD (10)
    ///
    /// Stores registers V0 through Vx in memory at
    /// location I
    fn opcode_ld10(&mut self, vx: usize) -> CpuReturn {
        let i = self.regs.i as usize;
        // We need to make a borrow on the ram so that we can use it in our for_each
        let ram = &mut self.ram;
        self.regs.v[0..vx].iter().enumerate().for_each(|(x, vx)| ram[i + x] = *vx);
        CpuReturn::Ok
    }

    /// Opcode: LD (11)
    ///
    /// Stores the RAM at I into V0 to Vx
    fn opcode_ld11(&mut self, vx: usize) -> CpuReturn {
        let i = self.regs.i as usize;
        let v = &mut self.regs.v;
        self.ram[i..i+vx].iter().enumerate().for_each(|(x, value)| v[x] = *value);
        CpuReturn::Ok
    }

    /// Opcode: ADD (1)
    ///
    /// Adds the value of Vx and the byte passed by the opcode
    /// and stores the result back in Vx
    fn opcode_add1(&mut self, vx: usize, byte: u8) -> CpuReturn {
        self.regs.v[vx] = self.regs.v[vx].wrapping_add(byte);
        CpuReturn::Ok
    }

    /// Opcode: ADD (2)
    ///
    /// Adds the values of Vx and Vy and stores the result in Vx
    /// Will set VF to 1 if a carry occurs, and 0 if no carry
    /// occurs
    fn opcode_add2(&mut self, vx: usize, vy: usize) -> CpuReturn {
        let result = u16::from(self.regs.v[vx]) + u16::from(self.regs.v[vy]);
        if result > u8::MAX.into() {
            self.regs.v[0xF] = 1;
        } else {
            self.regs.v[0xF] = 0;
        }
        self.regs.v[vx] = self.regs.v[vx].wrapping_add(self.regs.v[vy]);
        CpuReturn::Ok
    }

    /// Opcode: ADD (3)
    ///
    /// Adds the value of Vx and I and stores the result in I
    fn opcode_add3(&mut self, vx: usize) -> CpuReturn {
        self.regs.i += self.regs.v[vx];
        CpuReturn::Ok
    }

    /// Opcode: SUB
    ///
    /// Subtracts Vx and Vy and stores the result in Vx
    /// If Vx > Vy, then VF is set to 1, otherwise, VF is set to 0
    fn opcode_sub(&mut self, vx: usize, vy: usize) -> CpuReturn {
        if self.regs.v[vx] > self.regs.v[vy] {
            self.regs.v[0xF] = 1;
        } else {
            self.regs.v[0xF] = 0;
        }
        self.regs.v[vx] = self.regs.v[vx].wrapping_sub(self.regs.v[vy]);
        CpuReturn::Ok
    }

    /// Opcode: SUBN
    ///
    /// Subtracts Vy and Vx and stores the result in Vx
    /// If Vy > Vx then VF is set to 1, otherwise, VF is set to 0
    fn opcode_subn(&mut self, vx: usize, vy: usize) -> CpuReturn {
        if self.regs.v[vy] > self.regs.v[vx] {
            self.regs.v[0xF] = 1;
        } else {
            self.regs.v[0xF] = 0;
        }
        self.regs.v[vx] = self.regs.v[vy].wrapping_sub(self.regs.v[vx]);
        CpuReturn::Ok
    }

    /// Opcode: OR
    ///
    /// ORs the values in Vx and Vy and stores the result in Vx
    fn opcode_or(&mut self, vx: usize, vy: usize) -> CpuReturn {
        self.regs.v[vx] |= self.regs.v[vy];
        CpuReturn::Ok
    }

    /// Opcode: AND
    ///
    /// ANDs the values in Vx and Vy and stores the result in Vx
    fn opcode_and(&mut self, vx: usize, vy: usize) -> CpuReturn {
        self.regs.v[vx] &= self.regs.v[vy];
        CpuReturn::Ok
    }

    /// Opcode: XOR
    ///
    /// XORs the values in Vx and Vy and stores the result in Vx
    fn opcode_xor(&mut self, vx: usize, vy: usize) -> CpuReturn {
        self.regs.v[vx] ^= self.regs.v[vy];
        CpuReturn::Ok
    }

    /// Opcode: SHR (Cowgod)
    ///
    /// Shifts Vx to the right one bit and stores it in Vx
    /// Stores the least significant bit in VF
    ///
    /// **NOTE**: The cowgod reference lists it as
    /// Vx = Vx >> 1, but after more research, it should be
    /// Vx = Vy >> 1. If you want to have the correct implementation,
    /// disable the "cowgod" feature
    #[cfg(feature = "cowgod")]
    fn opcode_shr(&mut self, vx: usize, vy: usize) -> CpuReturn {
        self.regs.v[0xF] = self.regs.v[vx] & 0x1;
        self.regs.v[vx] = self.regs.v[vx] >> 1;
        CpuReturn::Ok
    }

    /// Opcode: SHR (Original)
    ///
    /// Shifts Vy to the right one bit and stores it in Vx
    /// Stores the least significant bit in VF
    ///
    /// **NOTE**: The cowgod reference lists it as
    /// Vx = Vx >> 1, but after more research, it should be
    /// Vx = Vy >> 1. If you want to remain with Cowgod's
    /// technical implementation, enable the "cowgod" feature,
    #[cfg(not(feature = "cowgod"))]
    fn opcode_shr(&mut self, vx: usize, vy: usize) -> CpuReturn {
        self.regs.v[0xF] = self.regs.v[vy] & 0x1;
        self.regs.v[vx] = self.regs.v[vy] >> 1;

        CpuReturn::Ok
    }

    /// Opcode: SHL (Cowgod)
    ///
    /// Shifts Vx to the right one bit and stores it in Vx
    /// Stores the most significant bit in VF
    ///
    /// **NOTE**: The cowgod reference lists it as
    /// Vx = Vx << 1, but after more research, it should be
    /// Vx = Vy << 1. If you want to have the correct implementation,
    /// disable the "cowgod" feature
    #[cfg(feature = "cowgod")]
    fn opcode_shl(&mut self, vx: usize, vy: usize) -> CpuReturn {
        self.regs.v[0xF] = self.regs.v[vx] >> 7;
        self.regs.v[vx] = self.regs.v[vx] >> 1;
        CpuReturn::Ok
    }

    /// Opcode: SHL (Original)
    ///
    /// Shifts Vy to the right one bit and stores it in Vx
    /// Stores the least significant bit in VF
    ///
    /// **NOTE**: The cowgod reference lists it as
    /// Vx = Vx << 1, but after more research, it should be
    /// Vx = Vy << 1. If you want to remain with Cowgod's
    /// technical implementation, enable the "cowgod" feature,
    #[cfg(not(feature = "cowgod"))]
    fn opcode_shl(&mut self, vx: usize, vy: usize) -> CpuReturn {
        self.regs.v[0xF] = self.regs.v[vy] >> 7;
        self.regs.v[vx] = self.regs.v[vy] << 1;
        CpuReturn::Ok
    }

    /// Opcode: RND
    ///
    /// Genreates a random byte and ANDs that value with the byte passed
    /// and is stored into Vx
    fn opcode_rnd(&mut self, vx: usize, byte: u8) -> CpuReturn {
        self.regs.v[vx] = self.regs.rng.generate::<u8>() & byte;
        CpuReturn::Ok
    }

    /// Opcode: DRW
    ///
    /// Draws the sprite with bytes of n at memory location I at
    /// coordinates (Vx, Vy)
    ///
    /// VF is set to 1 if any collision occurs.
    /// The starting position of the sprite is wrapped around the screen,
    /// but the sprite drawing is not
    fn opcode_drw(&mut self, vx: usize, vy: usize, n: usize) -> CpuReturn {
        // Get the X and Y coordinate
        let x = self.regs.v[vx] as usize % self.disp.get_max_width();
        let y = self.regs.v[vy] as usize % self.disp.get_max_height();
        self.regs.v[0xF] = 0;
        for row in 0..n {
            let row_data = self.ram[(self.regs.i as usize) + n];
            for bit in 0..8 {
                if row_data & (1 << bit) != 0 {
                    if self.disp.draw_pixel(x + bit, y + row) {
                        self.regs.v[0xF] = 1;
                    }
                }
            }
        }
        CpuReturn::Ok
    }

    /// Opcode: SKP
    ///
    /// Will skip one instruction if the key Vx is currently pressed
    fn opcode_skp(&mut self, vx: usize) -> CpuReturn {
        if self.regs.v[vx] > 0xF {
            CpuReturn::Error((self.regs.pc, CpuError::InvalidValue(u16::from(self.regs.v[vx]))))
        } else {
            if self.keypad.is_key_pressed(self.regs.v[vx]) {
                self.regs.pc += 2;
            }
            CpuReturn::Ok
        }
    }

    /// Opcode: SKNP
    ///
    /// Will skip one instruction if the key Vx is not currently pressed
    fn opcode_sknp(&mut self, vx: usize) -> CpuReturn {
        if self.regs.v[vx] > 0xF {
            CpuReturn::Error((self.regs.pc, CpuError::InvalidValue(u16::from(self.regs.v[vx]))))
        } else {
            if !self.keypad.is_key_pressed(self.regs.v[vx]) {
                self.regs.pc += 2;
            }
            CpuReturn::Ok
        }
    }

    /// Execute opcode
    ///
    /// Executes a singular opcode. Does NOT increment the program counter
    fn execute_opcode(&mut self, opcode: u16) -> CpuReturn {
        match opcode & 0xF000 {
            // 0xXXXX
            // System opcodes
            0x0000 => {
                match opcode & 0x0FFF {
                    // 00E0 - CLS
                    0x0e0 => self.opcode_cls(),
                    // 00EE - RET
                    0x0ee => self.opcode_ret(),
                    // 0nnn - SYS addr
                    e => self.opcode_sys(e),
                }
            }
            // 1nnn - JP addr (1)
            0x1000 => self.opcode_jp1(Self::get_opcode_nnn(opcode) as usize),
            // 2nnn - CALL addr
            0x2000 => self.opcode_call(Self::get_opcode_nnn(opcode) as usize),
            // 3xkk - SE Vx, byte (1)
            0x3000 => self.opcode_se1(Self::get_opcode_x(opcode), Self::get_opcode_kk(opcode)),
            // 4xkk - SNE Vx, byte (1)
            0x4000 => self.opcode_sne1(Self::get_opcode_x(opcode), Self::get_opcode_kk(opcode)),
            // 5xy0 - SE Vx, Vy (2)
            0x5000 => {
                if opcode & 0xF == 0 {
                    self.opcode_se2(Self::get_opcode_x(opcode), Self::get_opcode_y(opcode))
                } else {
                    CpuReturn::Error((self.regs.pc, CpuError::InvalidOpcode(opcode)))
                }
            }
            // 0x6xkk - LD Vx, byte (1)
            0x6000 => self.opcode_ld1(Self::get_opcode_x(opcode), Self::get_opcode_kk(opcode)),

            // 0x7xkk - ADD Vx, byte (1)
            0x7000 => self.opcode_add1(Self::get_opcode_x(opcode), Self::get_opcode_kk(opcode)),
            // The 0x8xyo list of opcodes
            // All of these opcodes work on one or two of
            // these values:
            // - x is the first register and usually contains the
            //   result of the operation
            // - y is the second register and is usually just a
            //   secondary operator
            // - o is the operation to do
            0x8000 => {
                match Self::get_opcode_n(opcode) {
                    // 0x8xy0 - LD Vx, Vy (2)
                    0x0 => self.opcode_ld2(Self::get_opcode_x(opcode), Self::get_opcode_y(opcode)),
                    // 0x8xy1 - OR Vx, Vy
                    0x1 => self.opcode_or(Self::get_opcode_x(opcode), Self::get_opcode_y(opcode)),
                    // 0x8xy2 - AND Vx, Vy
                    0x2 => self.opcode_and(Self::get_opcode_x(opcode), Self::get_opcode_y(opcode)),
                    // 0x8xy3 - XOR Vx, Vy
                    0x3 => self.opcode_xor(Self::get_opcode_x(opcode), Self::get_opcode_y(opcode)),
                    // 0x8xy4 - ADD Vx, Vy (2)
                    0x4 => self.opcode_add2(Self::get_opcode_x(opcode), Self::get_opcode_y(opcode)),
                    // 0x8xy5 - SUB Vx, Vy
                    0x5 => self.opcode_sub(Self::get_opcode_x(opcode), Self::get_opcode_y(opcode)),
                    // 0x8xy6 - SHR Vx, Vy
                    0x6 => self.opcode_shr(Self::get_opcode_x(opcode), Self::get_opcode_y(opcode)),
                    // 0x8xy7 - SUBN Vx, Vy
                    0x7 => self.opcode_subn(Self::get_opcode_x(opcode), Self::get_opcode_y(opcode)),
                    // 0x8xyE - SHL Vx, Vy
                    0xE => self.opcode_shl(Self::get_opcode_x(opcode), Self::get_opcode_y(opcode)),
                    _ => {
                        CpuReturn::Error((self.regs.pc, CpuError::InvalidOpcode(opcode)))
                    }
                }
            }

            // 0x9xy0 - SNE Vx, Vy (2)
            0x9000 => {
                if opcode & 0xF == 0 {
                    self.opcode_sne2(Self::get_opcode_x(opcode), Self::get_opcode_y(opcode))
                } else {
                    CpuReturn::Error((self.regs.pc, CpuError::InvalidOpcode(opcode)))
                }
            },

            // 0xAnnn - LD I, addr (3)
            0xA000 => self.opcode_ld3(Self::get_opcode_nnn(opcode)),

            // 0xBnnn - JP V0, addr (2)
            0xB000 => self.opcode_jp2(Self::get_opcode_nnn(opcode) as usize),

            // 0xCxkk - RND Vx, byte
            0xC000 => self.opcode_rnd(Self::get_opcode_x(opcode), Self::get_opcode_kk(opcode)),

            // 0xDxyn - DRW Vx, Vy, nibble
            0xD000 => self.opcode_drw(Self::get_opcode_x(opcode), Self::get_opcode_y(opcode), Self::get_opcode_n(opcode) as usize),

            // 0xExoo instructions:
            0xE000 => {
                match opcode & 0xff {
                    // 0xEx9E - SKP Vx
                    0x9E => self.opcode_skp(Self::get_opcode_x(opcode)),
                    // 0xExA1 - SKNP Vx
                    0xA1 => self.opcode_sknp(Self::get_opcode_x(opcode)),
                    _ => CpuReturn::Error((self.regs.pc, CpuError::InvalidOpcode(opcode))),
                }
            }

            // 0xFxoo instructions:
            0xF000 => {
                match opcode & 0xFF {
                    // 0xFx07 - LD Vx, DT
                    0x07 => self.opcode_ld4(Self::get_opcode_x(opcode)),
                    // 0xFx0A - LD Vx, K
                    0x0A => self.opcode_ld5(Self::get_opcode_x(opcode)),
                    // 0xFx15 - LD DT, Vx
                    0x15 => self.opcode_ld6(Self::get_opcode_x(opcode)),
                    // 0xFx18 - LD ST, Vx
                    0x18 => self.opcode_ld7(Self::get_opcode_x(opcode)),
                    // 0xFx1E - ADD I, Vx
                    0x1E => self.opcode_add3(Self::get_opcode_x(opcode)),
                    // 0xFx29 - LD F, Vx
                    0x29 => self.opcode_ld8(Self::get_opcode_x(opcode)),
                    // 0xFx33 - LD B, Vx
                    0x33 => self.opcode_ld9(Self::get_opcode_x(opcode)),
                    // 0xFx55 - LD [I], Vx
                    0x55 => self.opcode_ld10(Self::get_opcode_x(opcode)),
                    // 0xFx65 - LD Vx, [I]
                    0x65 => self.opcode_ld11(Self::get_opcode_x(opcode)),
                    _ => CpuReturn::Error((self.regs.pc, CpuError::InvalidOpcode(opcode))),
                }
            }
            _ => CpuReturn::Error((self.regs.pc, CpuError::InvalidOpcode(opcode))),
        }
    }
}

struct Registers {
    v: [u8; 16],
    i: u16,
    delay: usize,
    sound: usize,
    //Program counter keeps track of where in the program we are
    pc: usize,
    stack: [usize; Self::STACK_SIZE],
    sp: usize,
    rng: nanorand::WyRand,
}

impl Registers {
    const STACK_SIZE: usize = 0x100;
    pub fn new() -> Registers {
        Registers {
            v: [0; 16],
            i: 0,
            delay: 0,
            sound: 0,
            pc: 0x200,
            stack: [0; Self::STACK_SIZE],
            sp: 0,
            rng: nanorand::WyRand::new(),
        }
    }
    pub fn reset(&mut self) {
        for v in &mut self.v {
            *v = 0
        }
        self.i = 0;
        self.delay = 0;
        self.sound = 0;
        self.pc = 0x200;
        self.sp = 0;
        self.rng = nanorand::WyRand::new();
    }
    pub fn push_to_stack(&mut self, data: usize) -> Result<(), ()> {
        if self.sp == Self::STACK_SIZE - 1 {
            Result::Err(())
        } else {
            self.stack[self.sp] = data;
            self.sp += 1;
            Result::Ok(())
        }
    }
    pub fn pop_from_stack(&mut self) -> Result<usize, ()> {
        if self.sp == 0 {
            Result::Err(())
        } else {
            self.sp -= 1;
            Result::Ok(self.stack[self.sp])
        }
    }
}
