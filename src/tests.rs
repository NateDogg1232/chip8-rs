#![allow(non_snake_case)]
#![allow(unreachable_code)]

use crate::CpuReturn;

struct SimpleDisplay {
    is_clear: bool,
    vram: [[bool; Self::DISPLAY_HEIGHT]; Self::DISPLAY_WIDTH],
}

impl SimpleDisplay {
    fn new() -> Self {
        Self {
            is_clear: false,
            vram: [[false; Self::DISPLAY_HEIGHT]; Self::DISPLAY_WIDTH]
        }
    }
    const DISPLAY_WIDTH: usize = 64;
    const DISPLAY_HEIGHT: usize = 32;
}

impl crate::Display for SimpleDisplay  {
    // The implementation of this is just accurate enough
    fn draw_pixel(&mut self, x: usize, y: usize) -> bool {
        self.is_clear = false;
        if self.vram[x][y] == true {
            self.vram[x][y] = false;
            false
        } else {
            self.vram[x][y] = true;
            true
        }
    }
    fn reset(&mut self) {
        self.is_clear = true;
    }
    fn clear(&mut self) {
        self.is_clear = true;
        for col in self.vram.iter_mut() {
            for pixel in col {
                *pixel = false;
            }
        }
    }
}

#[test]
pub fn invalid_opcode() {
    let mut disp = SimpleDisplay::new();
    let mut cpu = crate::Cpu::new(&mut disp);
    match cpu.execute_opcode(0xFAAF) {
        CpuReturn::Error((_, e)) => {
            assert_eq!(e, crate::CpuError::InvalidOpcode(0xFAAF));
        }
        _ => {
            // No error was thrown so we panic
            panic!("No error thrown");
        }
    };
}

#[test]
pub fn op_0nnn_sys() {
    let mut disp = SimpleDisplay::new();
    let mut cpu = crate::Cpu::new(&mut disp);
    match cpu.execute_opcode(0x0ded) {
        CpuReturn::Warn((_, w)) => {
            assert_eq!(w, crate::CpuWarning::SysOpcode(0x0ded));
        }
        e => {
            panic!("Opcode returned non-warning: {:?}", e);
        }
    }
}

#[test]
pub fn op_00e0_cls() {
    let mut disp = SimpleDisplay::new();
    let mut cpu = crate::Cpu::new(&mut disp);
    //First, draw on the display
    cpu.disp.draw_pixel(0, 0);
    match cpu.execute_opcode(0x00E0) {
        CpuReturn::Ok => {
            //Check if the display was cleared
            assert!(disp.is_clear) }
        e => {
            panic!("Opcode returned non-okay: {:?}", e);
        }
    }
}

#[test]
pub fn op_00ee_ret() {
    let mut disp = SimpleDisplay::new();
    let mut cpu = crate::Cpu::new(&mut disp);
    //Give it a dummy address
    cpu.regs.push_to_stack(0xdead_dad).unwrap();
    match cpu.execute_opcode(0x00EE) {
        CpuReturn::Ok => {
            assert_eq!(cpu.regs.pc, 0xdead_dad);
        }
        e => {
            panic!("Opcode returned non-okay: {:?}", e);
        }
    }
}

#[test]
pub fn op_00ee_ret_underflow() {
    let mut disp = SimpleDisplay::new();
    let mut cpu = crate::Cpu::new(&mut disp);
    match cpu.execute_opcode(0x00EE) {
        CpuReturn::Error((_, e)) => assert_eq!(e, crate::CpuError::StackUnderflow),
        e => {
            panic!("Opcode returned non-error: {:?}", e);
        }
    }
}

#[test]
pub fn op_1nnn_jp() {
    let mut disp = SimpleDisplay::new();
    let mut cpu = crate::Cpu::new(&mut disp);
    match cpu.execute_opcode(0x1add) {
        CpuReturn::Ok => {
            assert_eq!(cpu.regs.pc, 0xadd);
        }
        e => {
            panic!("Opcode returned non-okay: {:?}", e);
        }
    }
}

#[test]
pub fn op_2nnn_call() {
    let mut disp = SimpleDisplay::new();
    let mut cpu = crate::Cpu::new(&mut disp);
    //Set the current program counter to something
    cpu.regs.pc = 0xdead_dad;
    match cpu.execute_opcode(0x2add) {
        CpuReturn::Ok => {
            assert_eq!(cpu.regs.stack[0], 0xdead_dad);
            assert_eq!(cpu.regs.pc, 0xadd);
        }
        e => {
            panic!("Opcode returned non-okay: {:?}", e);
        }
    }
}

#[test]
pub fn op_3xkk_se_eq() {
    let mut disp = SimpleDisplay::new();
    let mut cpu = crate::Cpu::new(&mut disp);
    cpu.regs.pc = 0xdead_dad;
    cpu.regs.v[0] = 0xd8;
    match cpu.execute_opcode(0x30d8) {
        CpuReturn::Ok => {
            assert_eq!(cpu.regs.pc, 0xdead_daf);
        }
        e => {
            panic!("Opcode returned non-okay: {:?}", e);
        }
    }
}

#[test]
pub fn op_3xkk_se_neq() {
    let mut disp = SimpleDisplay::new();
    let mut cpu = crate::Cpu::new(&mut disp);
    cpu.regs.pc = 0xdead_dad;
    cpu.regs.v[0] = 0xd8;
    match cpu.execute_opcode(0x30ea) {
        CpuReturn::Ok => {
            assert_eq!(cpu.regs.pc, 0xdead_dad);
        }
        e => {
            panic!("Opcode returned non-okay: {:?}", e);
        }
    }
}

#[test]
pub fn op_4xkk_sne_eq() {
    let mut disp = SimpleDisplay::new();
    let mut cpu = crate::Cpu::new(&mut disp);
    cpu.regs.pc = 0xdead_dad;
    cpu.regs.v[0] = 0xd8;
    match cpu.execute_opcode(0x40d8) {
        CpuReturn::Ok => {
            assert_eq!(cpu.regs.pc, 0xdead_dad);
        }
        e => {
            panic!("Opcode returned non-okay: {:?}", e);
        }
    }
}

#[test]
pub fn op_4xkk_sne_neq() {
    let mut disp = SimpleDisplay::new();
    let mut cpu = crate::Cpu::new(&mut disp);
    cpu.regs.pc = 0xdead_dad;
    cpu.regs.v[0] = 0xd8;
    match cpu.execute_opcode(0x40ea) {
        CpuReturn::Ok => {
            assert_eq!(cpu.regs.pc, 0xdead_daf);
        }
        e => {
            panic!("Opcode returned non-okay: {:?}", e);
        }
    }
}

#[test]
pub fn op_5xy0_se_eq() {
    let mut disp = SimpleDisplay::new();
    let mut cpu = crate::Cpu::new(&mut disp);
    cpu.regs.pc = 0xdead_dad;
    cpu.regs.v[0] = 0xd8;
    cpu.regs.v[1] = 0xd8;
    match cpu.execute_opcode(0x5010) {
        CpuReturn::Ok => {
            assert_eq!(cpu.regs.pc, 0xdead_daf);
        }
        e => {
            panic!("Opcode returned non-okay: {:?}", e);
        }
    }
}
#[test]
pub fn op_5xy0_se_neq() {
    let mut disp = SimpleDisplay::new();
    let mut cpu = crate::Cpu::new(&mut disp);
    cpu.regs.pc = 0xdead_dad;
    cpu.regs.v[0] = 0xd8;
    cpu.regs.v[1] = 0xea;
    match cpu.execute_opcode(0x5010) {
        CpuReturn::Ok => {
            assert_eq!(cpu.regs.pc, 0xdead_dad);
        }
        e => {
            panic!("Opcode returned non-okay: {:?}", e);
        }
    }
}

#[test]
pub fn op_5xy0_se_ending0() {
    let mut disp = SimpleDisplay::new();
    let mut cpu = crate::Cpu::new(&mut disp);
    match cpu.execute_opcode(0x5004) {
        CpuReturn::Error((_, e)) => {
            assert_eq!(e, crate::CpuError::InvalidOpcode(0x5004));
        }
        _ => {
            panic!("No error thrown");
        }
    };
}

#[test]
pub fn op_6xkk_ld() {
    let mut disp = SimpleDisplay::new();
    let mut cpu = crate::Cpu::new(&mut disp);
    match cpu.execute_opcode(0x6dad) {
        CpuReturn::Ok => {
            assert_eq!(cpu.regs.v[0xd], 0xad);
        }
        e => {
            panic!("Opcode returned non-okay: {:?}", e);
        }
    }
}

#[test]
pub fn op_7xkk_add() {
    let mut disp = SimpleDisplay::new();
    let mut cpu = crate::Cpu::new(&mut disp);
    // Set up a register to have a known value
    cpu.regs.v[0xd]=0x11;
    match cpu.execute_opcode(0x7dad) {
        CpuReturn::Ok => {
            assert_eq!(cpu.regs.v[0xd], 0xbe);
        }
        e => {
            panic!("Opcode returned non-okay: {:?}", e);
        }
    }
}

#[test]
pub fn op_7xkk_add_wrap() {
    let mut disp = SimpleDisplay::new();
    let mut cpu = crate::Cpu::new(&mut disp);
    cpu.regs.v[0xd]=0xad;
    match cpu.execute_opcode(0x7dad) {
        CpuReturn::Ok => {
            assert_eq!(cpu.regs.v[0xd], 0x5a);
        },
        e => {
            panic!("Opcode returned non-ok: {:?}", e);
        }
    }
}

#[test]
pub fn op_8xy0_ld() {
    let mut disp = SimpleDisplay::new();
    let mut cpu = crate::Cpu::new(&mut disp);
    cpu.regs.v[0xd] = 0xba;
    cpu.regs.v[0xa] = 0xab;
    match cpu.execute_opcode(0x8da0) {
        CpuReturn::Ok => {
            assert_eq!(cpu.regs.v[0xd], 0xab);
        },
        e => {
            panic!("Opcode returned non-ok: {:?}", e);
        }
    }
}

#[test]
pub fn op_8xy1_or() {
    let mut disp = SimpleDisplay::new();
    let mut cpu = crate::Cpu::new(&mut disp);
    cpu.regs.v[0xd] = 0xba;
    cpu.regs.v[0xa] = 0x69;
    match cpu.execute_opcode(0x8da1) {
        CpuReturn::Ok => {
            assert_eq!(cpu.regs.v[0xd], 0xfb);
        },
        e => {
            panic!("Opcode returned non-ok: {:?}", e);
        }
    }
}

#[test]
pub fn op_8xy2_and() {
    let mut disp = SimpleDisplay::new();
    let mut cpu = crate::Cpu::new(&mut disp);
    cpu.regs.v[0xd] = 0xba;
    cpu.regs.v[0xa] = 0x69;
    match cpu.execute_opcode(0x8da2) {
        CpuReturn::Ok => {
            assert_eq!(cpu.regs.v[0xd], 0x28);
        },
        e => {
            panic!("Opcode returned non-ok: {:?}", e);
        }
    }
}

#[test]
pub fn op_8xy3_xor() {
    let mut disp = SimpleDisplay::new();
    let mut cpu = crate::Cpu::new(&mut disp);
    cpu.regs.v[0xd] = 0xba;
    cpu.regs.v[0xa] = 0x69;
    match cpu.execute_opcode(0x8da3) {
        CpuReturn::Ok => {
            assert_eq!(cpu.regs.v[0xd], 0xd3);
        },
        e => {
            panic!("Opcode returned non-ok: {:?}", e);
        }
    }
}

#[test]
pub fn op_8xy4_add_nocarry() {
    let mut disp = SimpleDisplay::new();
    let mut cpu = crate::Cpu::new(&mut disp);
    cpu.regs.v[0xd] = 0x69;
    cpu.regs.v[0xa] = 0x69;
    match cpu.execute_opcode(0x8da4) {
        CpuReturn::Ok => {
            assert_eq!(cpu.regs.v[0xd], 0xd2);
            assert_eq!(cpu.regs.v[0xf], 0);
        },
        e => {
            panic!("Opcode returned non-ok: {:?}", e);
        }
    }
}
#[test]
pub fn op_8xy4_add_carry() {
    let mut disp = SimpleDisplay::new();
    let mut cpu = crate::Cpu::new(&mut disp);
    cpu.regs.v[0xd] = 0xba;
    cpu.regs.v[0xa] = 0x69;
    match cpu.execute_opcode(0x8da4) {
        CpuReturn::Ok => {
            assert_eq!(cpu.regs.v[0xd], 0x23);
            assert_eq!(cpu.regs.v[0xf], 1);
        },
        e => {
            panic!("Opcode returned non-ok: {:?}", e);
        }
    }
}

#[test]
pub fn op_8xy5_sub_nocarry() {
    let mut disp = SimpleDisplay::new();
    let mut cpu = crate::Cpu::new(&mut disp);
    cpu.regs.v[0xd] = 0xba;
    cpu.regs.v[0xa] = 0x69;
    match cpu.execute_opcode(0x8da5) {
        CpuReturn::Ok => {
            assert_eq!(cpu.regs.v[0xd], 0x51);
            assert_eq!(cpu.regs.v[0xf], 1);
        },
        e => {
            panic!("Opcode returned non-ok: {:?}", e);
        }
    }
}
#[test]
pub fn op_8xy5_sub_carry() {
    let mut disp = SimpleDisplay::new();
    let mut cpu = crate::Cpu::new(&mut disp);
    cpu.regs.v[0xa] = 0xba;
    cpu.regs.v[0xd] = 0x69;
    match cpu.execute_opcode(0x8da5) {
        CpuReturn::Ok => {
            assert_eq!(cpu.regs.v[0xd], 0xaf);
            assert_eq!(cpu.regs.v[0xf], 0);
        },
        e => {
            panic!("Opcode returned non-ok: {:?}", e);
        }
    }
}
