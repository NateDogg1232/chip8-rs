# Chip8-rs

A CHIP-8 emulation library written in rust

You can learn more about the CHIP-8 system [here](https://en.wikipedia.org/wiki/CHIP-8) and get the techincal specifications I'm using to write this [here](http://devernay.free.fr/hacks/chip8/C8TECH10.HTM)

## Goals:
- To be easy to interface with.
- To be able to be used in `no_std` environments

## To build:
- Install Rust using [rustup](https://rustup.rs/)
- Run `cargo build` in the directory.
- Run `cargo test` to run the built-in tests

## TODO:
[ ] Finish up the Opcodes
[ ] Write a basic implementation of this library
